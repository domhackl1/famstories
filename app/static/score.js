$(document).ready(function(){
   
    $("a.vote").on("click", function() {

        var clicked_obj = $(this); 
        var vote_id = $(this).attr('id');
        //console.log(vote_id);
        var vote_type = $(this).children()[0].id;
        
        
        $.ajax({
            url: '/voted',
            type: 'POST',
            data: JSON.stringify({ vote_id: vote_id, vote_type: vote_type}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(response){
                console.log(response);

                if(vote_type == "upvote"){
                    clicked_obj.children()[1].innerHTML = " " + response.upvotes;
                } else {
                    clicked_obj.children()[1].innerHTML = " " + response.downvotes;
                }
            },
            error: function(error){
                console.log(error);
            }

        })
    });
});