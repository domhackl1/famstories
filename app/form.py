from flask_wtf import Form
from wtforms.validators import DataRequired
from wtforms import SubmitField, StringField


# The form that is used for voting
class Vote(Form):
    vote = SubmitField('vote')
    location = StringField('location', validators=[DataRequired()])

