from flask import render_template, request, Flask, redirect, url_for, flash, jsonify
from app import app
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import InputRequired, Email, Length
from flask_bootstrap import Bootstrap
from flask_sqlalchemy  import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from locationdb import Locations
import json
import logging
from form import Vote
from sqlalchemy.orm import sessionmaker
import os
'''
from logging import FileHandler, WARNING
handling = FileHandler('errorslog.txt')
handling.setLevel(WARNING)
app.logger.addHandler(handling)
logging.warning("Whats up dude")
'''

bootstrap = Bootstrap(app)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SECRET_KEY'] = 'superdupersecretkey'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'users.db')
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/domhackl/uni/webdev/project2/famstories/app/users.db'
db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

class Users(UserMixin, db.Model):
        id = db.Column(db.Integer, primary_key = True)
        username = db.Column(db.String(15), unique = True)
        email = db.Column(db.String(30), unique = True)
        password = db.Column(db.String(80), unique = True)


@login_manager.user_loader
def load_user(user_id):
    return Users.query.get(int(user_id))

def test(self, session):
    with session.no_autoflush:
        q = session.query(X).filter(X._val == self._val)
        x = q.one()
        print('x={}'.format(x))

class LoginForm(FlaskForm):
        username = StringField("Username", validators=[InputRequired(), Length(min=4, max=18)])
        password = PasswordField("Password", validators=[InputRequired(), Length(min=8, max=80)])

class RegisterForm(FlaskForm):
    email = StringField('email', validators=[InputRequired(), Email(message='Not valid email'), Length(max=30)])
    username = StringField('username', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('password', validators=[InputRequired(), Length(min=8, max=80)])

@app.route('/')
@login_required
def index():
    return render_template('index.html',
                            title='Home Page',
                            name=current_user.username )


@app.route('/logout', methods=['GET','POST'])
@login_required
def logout():
    logout_user
    return redirect(url_for('loggedout'))


@app.route('/loggedout', methods=['GET','POST'])
@login_required
def loggedout():
    return render_template('loggedout.html')

@app.route('/vote', methods=['POST'])
def vote():
        session = sessionmaker()()
        form = Vote()
        if(form.validate_on_submit()):
                # Check if the vote submit button is in the form (indicates the user voted)
                if('vote' in request.form):
                        # The button that was pressed by the user
                        vote_type = request.form['vote']

                        loc = Locations.query.get(int(form.location.data))

                        if vote_type == 'Upvote':
                                loc.score = loc.score + 1
                        elif vote_type == 'Downvote':
                                if loc.score > 1:
                                        loc.score = loc.score - 1
                        # return db.sessionmaker(bind=engine, autoflush=False)()
                        try:
                                return db.sessionmaker(bind=engine, autoflush=False)()
                                session.commit()
                                #db.session.commit()
                                #db.session.no_autoflush()
                        except:
                                db.session.rollback()
        else:
                logging.warn('Form is not valid')
                logging.warn(str(form.errors))

                                

        return redirect('/listing')

'''
def upvoted():

        data = json.loads(request.data)
	vote_id = (data.get('vote_id'))
	vote = models.Locations.query.get(vote_id)

        if data.get('vote_type') == "upvote":
                vote.upvotes += 1
        else:
                vote.upvotes -= 1
        db.session.commit()
        return json.dumps({'status':'OK','upvotes': idea.upvotes, 'downvotes': idea.downvotes })

        if form.validate_on_submit():
                uid = Locations.query.filter_by(id = form.id.data).first()
                if form.
                return redirect(url_for('listing'))

'''




@app.route('/listing')
@login_required
def listing():
    form = Vote()
    myLocation = Locations.query.all()
    return render_template('locations.html', title='Locations', name=current_user.username, myLocation=myLocation,form=form )

@app.route('/login', methods=['GET','POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
            user = Users.query.filter_by(username = form.username.data).first()
            if user:
                if check_password_hash(user.password, form.password.data):
                        login_user(user)
                        return redirect(url_for('index'))

            flash("Incorrect login combination!")
            return redirect(url_for('login'))
       

    return render_template('login.html',
                            title='Log into to use the website',
                            form=form
                            )



@app.route('/signup', methods=['GET','POST'])
def register():
    form = RegisterForm()
    

    if form.validate_on_submit():
            hashed_pwd = generate_password_hash(form.password.data, method='sha256')
            new_user =  Users(username = form.username.data, email = form.email.data, password = hashed_pwd)
            db.session.add(new_user)
            db.session.commit()
            return render_template('signedup.html')

    return render_template('signup.html',
                            title='Sign Up to use the shop',
                            form=form
                            )
    


       

