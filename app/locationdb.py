from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/domhackl/uni/webdev/project2/famstories/app/users.db'
db = SQLAlchemy(app)

class Locations(db.Model):
        __tablename = 'Locations'
        id = db.Column('id', db.Integer, primary_key = True)
        name = db.Column('name', db.String(30), unique = True)
        detail = db.Column('detail', db.String(4000), unique = True)
        location = db.Column('location', db.String(500), unique = False)
        score = db.Column('score', db.Integer, unique = False)
        
        def __init__(self, id, name, detail, location, score):
                self.id = id
                self.name = name
                self.detail = detail
                self.location = location
                self.score = score

